<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//laravel's default route
Route::get('/', 'PostsController@home');

//to get all posts in a list view
Route::get('/posts', 'PostsController@index');

//to store a new post into a database
Route::post('/posts', 'PostsController@store')->middleware('admin');

//to get the form to create a new post
Route::get('/posts/create', 'PostsController@create')->middleware('admin');

//validation routs---laravel's default
Auth::routes();

//laravel's default to get home page of an authorized user
Route::get('/home', 'HomeController@index')->name('home');

//to get the detailed view of a post
Route::get('/posts/{post}', 'PostsController@show');

//to store the comment into the database
Route::post('/comments', 'CommentsController@store')->middleware('auth');

//to get the detailed view of a post
Route::get('/archives/{year}/{month}', 'PostsController@archive');

Route::get('/posts/{post}/edit', 'PostsController@edit')->middleware('admin');

Route::patch('/posts','PostsController@update')->middleware('admin');
