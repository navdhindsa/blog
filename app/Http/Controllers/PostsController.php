<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Carbon\Carbon;
class PostsController extends Controller
{
    /**
     * Display home page.
     * @return View directs to the home view
     */
    public function home()
    {
        $posts=Post::orderBy('id','desc')->latest()->limit(4)->get();
        
        return view('welcome', compact('posts'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response the list view from the index.blade.php
     */
    public function index()
    {
        $posts=Post::latest()->with('categories')->Paginate(10);

        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response the create posts view
     */
    public function create()
    {
        return view('posts.create2');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response redirects to the posts page
     */
    public function store(Request $request)
    {
        //validation using laravel's validator
      $request->validate([
        'title'=>'required',
        'body'=>'required'
      ]);
      
      //create a new post in the database using the post model
      Post::create([
        'title'=>$request->input('title'),
        'body'=>$request->input('body'),
        'feature_image'=>'default.jpg',
        'thumbnail_image'=>'default.jpg'
      ]);
      return redirect('/posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response the detail view of a post
     */
    public function show(Post $post)
    {
      return view('posts.show', compact('post'));  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {

        return view('posts.edit',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate(request(),[
            'title'=>'required',
            'body'=>'required',
            'id'=>'required'
        ]);
        $post=Post::find(request('id'));
        $post->title=request('title');
        $post->body=request('body');
        $post->save();
        return redirect('/posts/'.$post->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Gets the archive side bar
     * @param  [string] $year  
     * @param  [string] $month 
     * @return  \Illuminate\Http\Response view
     */
    public function archive($year, $month)
    {
        $posts = Post::filter(['year'=>$year,'month'=>$month])->Paginate(10);
        
        return view('posts.archive', compact('posts'));
    }
}
