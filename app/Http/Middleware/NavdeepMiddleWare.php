<?php

namespace App\Http\Middleware;

use Closure;

class NavdeepMiddleWare
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!\Auth::check() || !\Auth::user()->is_admin){
            session()->flash('message','You need to be an admin!!');
            return redirect('/');
        }
        return $next($request);
    }
}
