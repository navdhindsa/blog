<?php

namespace App;
use App\Comment;
use carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Category;

class Post extends Model
{
    //protected $fillable = ['title','body'];
    //defining our guarded array empty means allowing everything fillable
    protected $guarded = [];
  /**
   * it defines the relationship between post and comments
   * @return the one to many relation function hasMany()
   */
  public function comments()
  {
    return $this->hasMany(Comment::class);
  }

  public function scopeFilter($query, $filters)
  {
   

        return Post::latest()->whereYear('created_at',$filters['year'])
        ->whereMonth('created_at', Carbon::parse($filters['month'])->month);
  }

  public static function archives()
  {
    $archives = Post::selectRaw('year(created_at) year,monthname(created_at) month, count(*) count')->groupBy('year','month')->orderByRaw('min(created_at) desc ')->get()->toArray();

    return $archives;
  }
  public function categories()
  {
      return $this->belongsToMany(Category::class);
    }
}

