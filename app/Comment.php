<?php

namespace App;
use App\Post;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
  protected $table = 'comments';
  protected $guarded = [];

  /**
   * it defines the relationship between post and comments
   * @return the one to one relation function belongsTo()
   */
  public function post()
  {
    return $this->belongsTo('App\Post');
  }
  /**
   * it defines the relationship between user and comments
   * @return the one to one relation function belongsTo()
   */
  public function user()
  {
    return $this->belongsTo('App\User')->first();
  }
}
