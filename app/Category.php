<?php

namespace App;
use App\Post;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	public function posts()
	{
    	return $this->belongsToMany(Post::class);
    }
    public static function categories()
    {
    	$categories = Post::selectRaw('year(created_at) year,monthname(created_at) month, count(*) count')->groupBy('year','month')->orderByRaw('min(created_at) desc ')->get()->toArray();

    return $categories;
    }
}
