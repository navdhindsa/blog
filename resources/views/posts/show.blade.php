@extends('layouts.master')

@section('content')
                <!-- Blog Entries Column -->
        <div class="col-md-8">
          
          <!-- Title -->
          <h1 class="mt-4">{{$post->title}}</h1>

          <!-- Author -->
          <p class="lead">
            by
            <a href="/posts/{{$post->id}}/edit">Author</a>
          </p>
          
            @if(Auth::check()&&Auth::user()->is_admin)
            <a href="#" class="btn btn-primary">Edit</a>
            @endif
          <hr>

          <!-- Date/Time -->
          <p>Posted on {{$post->created_at->toFormattedDateString()}} at  {{$post->created_at->toTimeString()}}  </p>

          <hr>

          <!-- Preview Image -->
          <img class="img-fluid rounded" src="/images/feature/{{$post->feature_image}}" alt="">

          <hr>

          <!-- Post Content -->
          <p class="lead">{{$post->body}}</p>

          <hr>
          @if(Auth::check())
          <!-- Comments Form -->
          @include('layouts.partials.errors')
          <div class="card my-4">
            <h5 class="card-header">Leave a Comment:</h5>
            <div class="card-body">
              <form method="post" action="/comments">
                {{csrf_field()}}
                <input type="hidden" name="user_id" value="" />
                <input type="hidden" name="post_id" value="{{$post->id}}" />
                <div class="form-group">
                  <textarea class="form-control"  name="body" rows="3">{{old('body')}}</textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
            </div>
          </div>
          @else
          <div class="card my-4">
            <h5 class="card-header">Leave a Comment:</h5>
            <div class="card-body">
              You must be logged in to post a comment!!
            </div>
          </div>
          @endif
          
          @foreach($post->comments()->get() as $comment)
          <!-- Single Comment -->
          <div class="media mb-4">
            <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
            <div class="media-body">
              <h5 class="mt-0">{{$comment->user()->name}}</h5>
              {{$comment->body}}
            </div>
          </div>
          @endforeach
          

           
          
</div>
@endsection