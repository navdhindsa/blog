@extends('layouts.master')

@section('content')

        <!-- Blog Entries Column -->
        <div class="col-md-8">

          <h1 class="my-4">Edit Post</h1>
          
            {!! Form::model($post,['url' => '/posts', 'method'=>"PATCH"]) !!}
            @include('layouts.partials.errors')
          <div class="form-group">
            {!!Form::label('title', 'Post Title');!!}
            {!!Form::text('title');!!} 
            {!!Form::hidden('id',$post->id)!!}
          </div>
          <div class="form-group">
            {!!Form::label('body', 'Body');!!} 
            {!!Form::textarea('body')!!}

          </div>
          <div class="form-group">
            {!!Form::submit('Update!',  ['class'=>'btn btn-primary']);!!} 

          </div>
          
            
          {!! Form::close() !!}
            
            

          
        </div>

        

@endsection