@extends('layouts.master')

@section('content')

        <!-- Blog Entries Column -->
        <div class="col-md-8">

          <h1 class="my-4">Create a Post</h1>
          {!! Form::open(['url' => '/posts']) !!}
          <div class="form-group">
            {!!Form::label('title', 'Post Title');!!}
            {!!Form::text('title');!!} 

          </div>
          <div class="form-group">
            {!!Form::label('body', 'Body');!!} 
            {!!Form::textarea('body')!!}

          </div>
          <div class="form-group">
            {!!Form::submit('Publish!',  ['class'=>'btn btn-primary']);!!} 

          </div>
          
            
          {!! Form::close() !!}

          
        </div>

        

@endsection