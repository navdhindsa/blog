@extends('layouts.master')

@section('content')

        <!-- Blog Entries Column -->
        <div class="col-md-8">

          <h1 class="my-4">Edit Post</h1>
          @if(Auth::check())
          <h2>You are logged in !!</h2>
          @endif
          <form action="/posts" method="post">
            @include('layouts.partials.errors')
            <div class="form-group">
              @csrf
              <input type="hidden" name="_method" value="patch" />
              <input type="hidden" name="id" value="{{$post->id}}" />
              <label for="title">Title</label>
              <input type="text" name="title" class="form-control" id="exampleFormControlInput1" value="{{old('title', $post->title)}}" placeholder="Title">
            </div>
            
            <div class="form-group">
              <label for="body">Body</label>
              <textarea class="form-control"  name="body" id="exampleFormControlTextarea1" rows="3">{{old('body',$post->body)}}</textarea>
            </div>
            
            <div class="form-group">
              <button type="submit" class="btn btn-primary">Publish</button>
            </div>
          </form>

          
        </div>

        

@endsection