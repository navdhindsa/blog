@include('layouts.partials.header')

   @include('layouts.partials.nav')
   @include('layouts.partials.flash')
    <!-- Page Content -->
    <div class="container">
      <br />
      @if(isset($posts))
      <div id="carouselControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <a href="/posts/{{$posts[0]['id']}}"><img class="d-block w-100" src="images/feature/{{$posts[0]['feature_image']}}" alt="{{$posts[0]['title']}}"></a>
          </div>
          <div class="carousel-item">
            <a href="/posts/{{$posts[1]['id']}}"><img class="d-block w-100" src="images/feature/{{$posts[1]['feature_image']}}" alt="{{$posts[1]['title']}}"></a>
          </div>
          <div class="carousel-item">
            <a href="/posts/{{$posts[2]['id']}}"><img class="d-block w-100" src="images/feature/{{$posts[2]['feature_image']}}" alt="{{$posts[2]['title']}}"></a>
          </div>
          <div class="carousel-item">
            <a href="/posts/{{$posts[3]['id']}}"><img class="d-block w-100" src="images/feature/{{$posts[3]['feature_image']}}" alt="{{$posts[3]['title']}}"></a>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      @endif
      <div class="row">

        @yield('content')
      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->
    <br />
    @include('layouts.partials.footer')