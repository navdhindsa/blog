@include('layouts.partials.header')

   @include('layouts.partials.nav')
   
   @include('layouts.partials.flash')
    <!-- Page Content -->
    <div class="container">

      <div class="row">

        @yield('content')
        @include('layouts.partials.sidebar')
      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    @include('layouts.partials.footer')