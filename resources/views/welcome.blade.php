@extends('layouts.master_home')

@section('content')


      <!-- Page Features -->
      <br />
      <div class="row text-center">
        @foreach($posts as $post)
        <div class="col-lg-3 col-md-6 mb-4">
          <div class="card">
            <img class="card-img-top" src="images/thumb/{{$post['thumbnail_image']}}" alt="">
            <div class="card-body">
              <h4 class="card-title">{{$post['title']}}</h4>
              <p class="card-text">{{$post['excerpt']}}</p>
            </div>
            <div class="card-footer">
              <a href="/posts/{{$post['id']}}" class="btn btn-primary">Find Out More!</a>
            </div>
          </div>
        </div>
        @endforeach
        

      </div>

      <!-- /.row -->

@endsection