<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(\App\Post::class, function (Faker $faker) {
    return [
        'title'=> $faker->sentence,
        'body' => $faker->realtext,
        'feature_image'=> 'default.jpg',
        'thumbnail_image' => 'default.jpg',
        'created_at'=>$faker->dateTimeThisYear,
        'updated_at'=>Carbon::now()
    ];
});
