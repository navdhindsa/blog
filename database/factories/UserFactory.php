<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
DB::table('users')->insert([
        'name' => 'Navdeep Dhindsa',
        'email' => 'nav@gmail.com',
        'is_admin'=>true,
        'password' => bcrypt('mypass'), // secret
        'remember_token' => str_random(10),
        'created_at'=>Carbon::now(),
        'updated_at'=>Carbon::now()
    ]);
$factory->define(App\User::class, function (Faker $faker) {
	
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'is_admin'=>false,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'created_at'=>Carbon::now(),
        'updated_at'=>Carbon::now()
    ];
});
