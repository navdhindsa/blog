<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SeedCategoriesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert(
        	[
        		'name'=>'books',
        		'created_at' =>Carbon::now(),
        		'updated_at' =>Carbon::now()
        	]);
        DB::table('categories')->insert(
        	[
        		'name'=>'movies',
        		'created_at' =>Carbon::now(),
        		'updated_at' =>Carbon::now()
        	]);
        DB::table('categories')->insert(
        	[
        		'name'=>'travel',
        		'created_at' =>Carbon::now(),
        		'updated_at' =>Carbon::now()
        	]);
        DB::table('categories')->insert(
        	[
        		'name'=>'adventure',
        		'created_at' =>Carbon::now(),
        		'updated_at' =>Carbon::now()
        	]);
        DB::table('categories')->insert(
        	[
        		'name'=>'fashion',
        		'created_at' =>Carbon::now(),
        		'updated_at' =>Carbon::now()
        	]);
    }
}
