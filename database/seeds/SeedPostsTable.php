<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class SeedPostsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//5 manual post entries
    	DB::table('posts')->insert([
        'title' => 'Peep Toes - Beauty vs Comfort!',
        'excerpt' => 'Everyone likes beautiful toes peeping out from the high heels! But are they worth having the discomfort of blistering feet?',
        'body' => 'A peep-toe shoe is a woman\'s shoe (usually a pump, slingback, bootie, or any other dress shoe) in which there is an opening at the toe-box which allows the toes to show.

Peep-toe shoes were popular beginning in the 1940sbut disappeared by the 1960s. Peep-toe shoes had a brief resurgence in the 1970s/80s, before falling out of fashion by the mid-1990s. More recently, they have become popular again, with variations such as "peep-toe boots" appearing',
        'feature_image' => '1.jpg',
        'thumbnail_image' => '1.jpg',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    	]);

    	DB::table('posts')->insert([
        'title' => 'Black Pumps- A must have for your wardrobe!',
        'excerpt' => 'Whether aquick friend\'s house visit or an impromptu party, we all need a pair of black pumps in our closet! They always save the day!',
        'body' => 'Cultures have different customs regarding footwear. These include not using any in some situations, usually bearing a symbolic meaning. This can however also be imposed on specific individuals to place them at a practical disadvantage against shod people, if they are excluded from having footwear available or are prohibited from using any. This usually takes place in situations of captivity, such as imprisonment or slavery, where the groups are among other things distinctly divided by whether or whether not footwear is being worn. In these cases the use of footwear categorically indicates the exercise of power as against being devoid of footwear, evidently indicating inferiority.',
        'feature_image' => '2.jpg',
        'thumbnail_image' => '2.jpg',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    	]);

    	DB::table('posts')->insert([
        'title' => 'Red vs Black- tough Choice!',
        'excerpt' => 'Hmm, do u prefer the safe black or the dangerous red? tough choice right? So why not have both and save the trouble of choosing!!',
        'body' => 'Cultures have different customs regarding footwear. These include not using any in some situations, usually bearing a symbolic meaning. This can however also be imposed on specific individuals to place them at a practical disadvantage against shod people, if they are excluded from having footwear available or are prohibited from using any. This usually takes place in situations of captivity, such as imprisonment or slavery, where the groups are among other things distinctly divided by whether or whether not footwear is being worn. In these cases the use of footwear categorically indicates the exercise of power as against being devoid of footwear, evidently indicating inferiority.',
        'feature_image' => '3.jpg',
        'thumbnail_image' => '3.jpg',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    	]);

    	DB::table('posts')->insert([
        'title' => 'Strappy Sandals- Every girl\'s weakness!',
        'excerpt' => 'I love the cool Strappy Sandals. Having 3 pairs myself, they make you feel beautiful and there\'s simply no resisting them!!',
        'body' => 'Cultures have different customs regarding footwear. These include not using any in some situations, usually bearing a symbolic meaning. This can however also be imposed on specific individuals to place them at a practical disadvantage against shod people, if they are excluded from having footwear available or are prohibited from using any. This usually takes place in situations of captivity, such as imprisonment or slavery, where the groups are among other things distinctly divided by whether or whether not footwear is being worn. In these cases the use of footwear categorically indicates the exercise of power as against being devoid of footwear, evidently indicating inferiority.',
        'feature_image' => '4.jpg',
        'thumbnail_image' => '4.jpg',
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    	]);

       

    	//add your post here, don't forget to add the image in the feature and thumb folder in the public folder!! And don't forget the jpg

    	


        //factory(\App\Post::class, 50)->create();
    }
}
